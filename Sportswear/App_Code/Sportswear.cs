﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MongoDB.Bson;
using MongoDB.Driver;

/// <summary>
/// Summary description for Records
/// </summary>
public class Sportswear
{
    public int _id { get; set; }
    public string name { get; set; }
    public string imageURL { get; set; }
    public List<string> tags { get; set; } //pored standardnih tagova sadrzi i pol(muski, zenski, deciji) i sportove(tenis, trcanje, ...)
    public string description { get; set; }
    public List<string> size { get; set; }
    public string price { get; set; }
    public string currency { get; set; }

    public Sportswear()
    {
        tags = new List<string>();
        size = new List<string>();
    }

}