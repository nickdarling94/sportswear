db.products.insert(
{"_id":1, 
"name":"Nike Sports T-Shirt",
"imageURL":"1.jpg", 
"tags":["t_shirt", "nike", "tshirt", "sports", "sportswear", "sport", "men", "running"], 
"description":"Special edition sportswear T-Shirt!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":30.00
});

db.products.insert(
{"_id":2, 
"name":"Adidas Sports T-Shirt",
"imageURL":"2.jpg", 
"tags":["adidas", "tshirt", "sports", "sportswear", "sport", "t_shirt"], 
"description":"Special edition sportswear T-Shirt!",
"size":["S", "M", "L"],
"currency":"EUR",
"price":27.00
});

db.products.insert(
{"_id":3, 
"name":"Champion Sports T-Shirt",
"imageURL":"3.jpg", 
"tags":["t_shirt", "champion", "tshirt", "sports", "sportswear", "sport"], 
"description":"Special edition sportswear T-Shirt!",
"size":["M", "XL", "XXL"],
"currency":"EUR",
"price":32.99
});

db.products.insert(
{"_id":4, 
"name":"Adidas Sports Tracksuit",
"imageURL":"4.jpg", 
"tags":["tracksuit", "adidas", "sports", "blue", "sport", "kids", "tennis"], 
"description":"Sales of sports tracksuits!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":80.00
});

db.products.insert(
{"_id":5, 
"name":"Puma Sports Tracksuit",
"imageURL":"5.jpg", 
"tags":["puma", "sports", "red", "sport", "male", "training", "tracksuit"], 
"description":"Sales of sports tracksuits!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":100.00
});

db.products.insert(
{"_id":6, 
"name":"Reebok Sports Tracksuit",
"imageURL":"6.jpg", 
"tags":["reebok", "tracksuit", "sports", "green", "sport", "male", "running"], 
"description":"Sales of sports tracksuits!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":90.00
});

db.products.insert(
{"_id":7, 
"name":"Georgos Sports Swimwear",
"imageURL":"7.jpg", 
"tags":["georgos", "swimwear", "sports", "green", "sport", "kids", "swimming"], 
"description":"Special sales of sports swimwear!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":40.00
});

db.products.insert(
{"_id":8, 
"name":"Arena Sports Swimwear",
"imageURL":"8.jpg", 
"tags":["arena", "swimwear", "sports", "black", "sport", "male", "swimming"], 
"description":"The biggest sale of sports swimwear!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":20.00
});

db.products.insert(
{"_id":9, 
"name":"Adidas Sports Swimwear",
"imageURL":"9.jpg", 
"tags":["adidas", "swimwear", "sports", "pink", "sport", "female", "swimming"], 
"description":"Special sales of sports swimwear!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":20.00
});

db.products.insert(
{"_id":10, 
"name":"Nike Sports Sweatpants",
"imageURL":"10.jpg", 
"tags":["nike", "sweatpants", "sports", "black", "sport", "female", "running"], 
"description":"Special sales of sports sweatpants!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":50.00
});

db.products.insert(
{"_id":11, 
"name":"Nike Sports Sweatpants",
"imageURL":"11.jpg", 
"tags":["nike", "sweatpants", "sports", "black", "sport", "male", "running"], 
"description":"The biggest sale of sports sweatpants!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":40.00
});

db.products.insert(
{"_id":12, 
"name":"Nike Sports Sweatpants",
"imageURL":"12.jpg", 
"tags":["nike", "sweatpants", "sports", "blue", "sport", "male", "walking"], 
"description":"The biggest sale of sports sweatpants!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":40.00
});

db.products.insert(
{"_id":13, 
"name":"Nike Sports Hoodies",
"imageURL":"13.jpg", 
"tags":["nike", "hoodies", "sports", "black", "sport", "male", "running", "hoodie"], 
"description":"Special edition sportswear hoodies!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":30.00
});

db.products.insert(
{"_id":14, 
"name":"Adidas Sports Hoodies",
"imageURL":"14.jpg", 
"tags":["adidas", "hoodies", "sports", "black", "sport", "male", "running", "hoodie"], 
"description":"Special edition sportswear hoodies!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":50.00
});

db.products.insert(
{"_id":15, 
"name":"Adidas Sports Hoodies",
"imageURL":"15.jpg", 
"tags":["adidas", "hoodies", "sports", "white", "sport", "female", "training", "hoodie"], 
"description":"Special edition sportswear hoodies!",
"size":["M", "L", "XL"],
"currency":"EUR",
"price":40.00
});

db.products.insert(
{"_id":16, 
"name":"Nike Sports Footwear",
"imageURL":"16.jpg", 
"tags":["nike", "footwear", "sports", "pink", "sport", "female", "walking"], 
"description":"The biggest sale of sports footwear!",
"size":["38", "39", "40"],
"currency":"EUR",
"price":100.00
});

db.products.insert(
{"_id":17, 
"name":"Adidas Sports Footwear",
"imageURL":"17.jpg", 
"tags":["adidas", "footwear", "sports", "black", "sport", "male", "football"],
"description":"The biggest sale of sports footwear!",
"size":["43", "44"],
"currency":"EUR",
"price":90.00
});

db.products.insert(
{"_id":18, 
"name":"Reebok Sports Footwear",
"imageURL":"18.jpg", 
"tags":["reebok", "footwear", "sports", "blue", "sport", "kids", "football"], 
"description":"The biggest sale of sports footwear!",
"size":["30", "32", "34", "35"],
"currency":"EUR",
"price":110.00
});

db.products.insert(
{"_id":19, 
"name":"Nike Ski Goggles",
"imageURL":"19.jpg", 
"tags":["nike", "googles", "ski", "white", "sport", "male", "skiing", "other"], 
"description":"Best ski googles!",
"size":["M", "L"],
"currency":"EUR",
"price":50.00
});

db.products.insert(
{"_id":20, 
"name":"Adidas Sports Cap",
"imageURL":"20.jpg", 
"tags":["other", "adidas", "cap", "sports", "black", "sport", "male", "walking"], 
"description":"Special sale of sports caps!",
"size":["30"],
"currency":"EUR",
"price":20.00
});

db.products.insert(
{"_id":21, 
"name":"Nike Sports Football",
"imageURL":"21.jpg", 
"tags":["nike", "football", "sports", "white", "sport", "game"], 
"description":"New nike football!",
"size":["/"],
"currency":"EUR",
"price":30.00
});