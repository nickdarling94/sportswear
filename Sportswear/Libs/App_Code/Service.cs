﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using System.Net;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class Service : IService
{

    public string[] GetProductList(int page)
    {
        var connectionString = "mongodb://localhost/?safe=true";
        var server = MongoServer.Create(connectionString);
        var db = server.GetDatabase("sportswear");

        var collection = db.GetCollection<BsonDocument>("products");

        List<string> sportswear = new List<string>();

        foreach (BsonDocument doc in collection.FindAll())
        {
            sportswear.Add(doc.ToJson());
        }

        return sportswear.ToArray();
    }


    public string GetProductDetails(string id)
    {
        return string.Empty;
    }

    public string DeleteItem(string id)
    {
        var connectionString = "mongodb://localhost/?safe=true";

        var server = MongoServer.Create(connectionString);
        var db = server.GetDatabase("sportswear");

        var collection = db.GetCollection<BsonDocument>("products");

        var query = Query.EQ("_id", Int16.Parse(id));
        collection.Remove(query);

        return "{\"status\":\"true\"}";
    }

    public void SavePicture(string url)
    {
        string FileName = url.Substring(url.LastIndexOf('/') + 1);

        string Path = AppDomain.CurrentDomain.BaseDirectory + "Images\\";
        WebClient webClient = new WebClient();
        webClient.DownloadFile(url, Path + FileName);
    }

    public void AddProduct(string data)
    {
        var connectionString = "mongodb://localhost/?safe=true";

        var server = MongoServer.Create(connectionString);
        var db = server.GetDatabase("sportswear");


        var collection = db.GetCollection<Sportswear>("products");

        string[] splitData = data.Split('|');

        Sportswear product = new Sportswear();
      
        product.name = splitData[0];
        string[] splitTags = splitData[1].Split(',');
        for (int i = 0; i < splitTags.Length; i++)
            product.tags.Add(splitTags[i].ToLower());
        product.description = splitData[2];
        string[] splitSizes = splitData[3].Split(' ');
        for (int i = 0; i < splitSizes.Length; i++)
            product.size.Add(splitSizes[i].ToLower());
        product.price = splitData[4];
        product.currency = splitData[5];
        product.imageURL = splitData[6];
        product._id = Int32.Parse(splitData[7]);

        collection.Insert(product);
    }
}
