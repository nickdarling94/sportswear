﻿// this is for 2nd row's li offset from top. It means how much offset you want to give them with animation
var single_li_offset;
var current_opened_box;
var Arrays;
var Da;
var NoviId;
var attempt = 3; //Variable to count number of attempts

/***********************************************************************************************************/
/***********************************************************************************************************/

//Below function Executes on click of login button
function validate() {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    if (username == "admin" && password == "admin") {
        alert("Login successfully");
        window.location = "AdminPage.html"; //redirecting to other page
        return false;
    }
    else {
        attempt--;//Decrementing by one
        alert("You have left " + attempt + " attempt;");

        //Disabling fields after 3 attempts
        if (attempt == 0) {
            document.getElementById("username").disabled = true;
            document.getElementById("password").disabled = true;
            document.getElementById("submit").disabled = true;
            return false;
        }
    }
}

/***********************************************************************************************************/
/***********************************************************************************************************/


function OnProductClick(target, e) {

    var thisID = target.attr('id');
    //var $this = $(this);
    var id1 = target.attr("id1");

    var id = $('#wrap li').index(target);

    if (current_opened_box == id) // if user click a opened box li again you close the box and return back
    {
        $("#wrap .detail-view").slideUp("slow");
        return false;
    }
    $("#cart_wrapper").slideUp("slow");
    $("#wrap .detail-view").slideUp("slow");

    // save this id. so if user click a opened box li again you close the box.
    current_opened_box = id;
    Da = id1;

    var targetOffset = 0;

    // below conditions assumes that there are four li in one row and total rows are 4. How ever if you want to increase the rows you have to increase else-if conditions and if you want to increase li in one row, then you have to increment all value below. (if(id<=3)), if(id<=7) etc

    if (id <= 3)
        targetOffset = single_li_offset;
    else if (id <= 7)
        targetOffset = single_li_offset;
    else if (id <= 11)
        targetOffset = single_li_offset * 2;
    else if (id <= 15)
        targetOffset = single_li_offset * 3;
    else if ((id <= 19))
        targetOffset = single_li_offset * 4;

    $("html:not(:animated),body:not(:animated)").animate({ scrollTop: targetOffset }, 800, function () {

        $('#wrap #detail-' + thisID).slideDown(500);
        return false;
    });
}

function OnAddToCart(target) {

    var thisID = target.parent().parent().attr('id').replace('detail-', '');

    var itemname = target.parent().find('.item_name').html();
    var itemprice = target.parent().find('.price').html();

    if (include(Arrays, thisID)) {
        var price = $('#each-' + thisID).children(".shopp-price").find('em').html();
        var quantity = $('#each-' + thisID).children(".shopp-quantity").html();
        quantity = parseInt(quantity) + parseInt(1);

        var total = parseInt(itemprice) * parseInt(quantity);

        $('#each-' + thisID).children(".shopp-price").find('em').html(total);
        $('#each-' + thisID).children(".shopp-quantity").html(quantity);

        var prev_charges = $('.cart-total span').html();
        prev_charges = parseInt(prev_charges) - parseInt(price);

        prev_charges = parseInt(prev_charges) + parseInt(total);
        $('.cart-total span').html(prev_charges);

        $('#total-hidden-charges').val(prev_charges);
    }
    else {
        Arrays.push(thisID);

        var prev_charges = $('.cart-total span').html();
        prev_charges = parseInt(prev_charges) + parseInt(itemprice);

        $('.cart-total span').html(prev_charges);
        $('#total-hidden-charges').val(prev_charges);

        var Height = $('#cart_wrapper').height();
        $('#cart_wrapper').css({ height: Height + parseInt(45) });

        $('#cart_wrapper .cart-info').append('<div class="shopp" id="each-' + thisID + '"><div class="label">' + itemname + '</div><div class="shopp-price"> €<em>' + itemprice + '</em></div><span class="shopp-quantity"> 1</span><img src="/images/remove.png" class="remove" /><br class="all" /></div>');

    }
}



//Funkcija koja pozivu GetProductList metodu WCF servisa
function GetProductList(page) {
    $.ajax(
                {
                    type: "GET", //GET or POST or PUT or DELETE verb
                    url: "Service.svc/GetProductList", // Location of the service
                    data: '{"page":' + page + '}', //Data sent to server
                    contentType: "application/json; charset=utf-8", // content type sent to server
                    dataType: "json", //Expected data format from server
                    processdata: true, //True or False
                    success: function (msg) //On Successfull service call
                    {
                        GetProductListSucceeded(msg);
                    },
                    error: function (result) // When Service call fails
                    {
                        alert("Error loading products " + result.status + " " + result.statusText);
                    }
                }
            );
}

function GetProductListOnCategory(page, s_cat) {
    $.ajax(
                {
                    type: "GET", //GET or POST or PUT or DELETE verb
                    url: "Service.svc/GetProductList", // Location of the service
                    data: '{"page":' + page + '}', //Data sent to server
                    contentType: "application/json; charset=utf-8", // content type sent to server
                    dataType: "json", //Expected data format from server
                    processdata: true, //True or False
                    success: function (msg) //On Successfull service call
                    {
                        GetProductListOnCategorySucceeded(msg, s_cat)
                    },
                    error: function (result) // When Service call fails
                    {
                        alert("Error loading products " + result.status + " " + result.statusText);
                    }
                }
            );
}

function GetProductListOnCategorySucceeded(result, selected_category) {
    var productList = document.createElement("ul");
    var prevProductItem;
    var prevDetailView;

    $("#wrapul").empty();
    var selected_brojac = 0;

    for (var i = 0; i < result.length; i++) {
        try {
            var product = eval('(' + result[i] + ')');
        }
        catch (exception) {
            return;
        }

        if (product.tags.indexOf(selected_category) > -1) {
            var productItem = document.createElement("li");

            $(productItem).attr("id", i + 1);
            //$(productItem).html(product.name);
            $(productItem).attr("id1", product._id);

            var productImage = document.createElement("img");
            $(productImage).attr("src", "Images/" + product.imageURL);
            $(productImage).attr("class", "items");
            $(productImage).attr("height", 100);
            $(productImage).attr("alt", "");


            var productBr = document.createElement("br");
            $(productBr).attr("clear", "all");

            var productDiv = document.createElement("div");
            $(productDiv).html(product.name);
            $(productDiv).attr("id", product._id);

            $(productItem).append(productImage);
            $(productItem).append(productBr);
            $(productItem).append(productDiv);

            var detailView = document.createElement("div");
            $(detailView).attr("class", "detail-view");
            $(detailView).attr("id", "detail-" + (i + 1).toString());


            var closeX = document.createElement("div");
            $(closeX).attr("class", "close");
            $(closeX).attr("align", "right");

            var closeXA = document.createElement("a");
            $(closeXA).html("x");
            $(closeXA).attr("href", "javascript:void(0)");
            $(closeX).append(closeXA);

            var productImage = document.createElement("img");
            $(productImage).attr("src", "Images/" + product.imageURL);
            $(productImage).attr("class", "detail_images");
            $(productImage).attr("width", 300);
            $(productImage).attr("height", 300);
            $(productImage).attr("alt", "");

            var detailInfo = document.createElement("div");
            $(detailInfo).attr("class", "detail_info");

            var itemName = document.createElement("label");
            $(itemName).attr("class", "item_name");
            $(itemName).html(product.name);

            var productBr = document.createElement("br");
            $(productBr).attr("clear", "all");

            var desc = "";
            for (var j = 0; j < product.tags.length - 1; j++) {
                desc = desc + product.tags[j] + ", ";
            }
            desc = desc + product.tags[product.tags.length - 1];


            var productDesc = document.createElement("p");
            $(productDesc).html(desc);

            var productBr1 = document.createElement("br");
            $(productBr1).attr("clear", "all");

            var productBr2 = document.createElement("br");
            $(productBr2).attr("clear", "all");

            var productSpan = document.createElement("span");
            $(productSpan).attr("class", "price");
            $(productSpan).html(product.price);

            var currSpan = document.createElement("span");
            $(currSpan).attr("class", "currency");
            $(currSpan).html(product.currency);

            $(productDesc).append(productBr1);
            $(productDesc).append(productBr2);
            $(productDesc).append(productSpan);
            $(productDesc).append(currSpan);

            var productBr3 = document.createElement("br");
            $(productBr3).attr("clear", "all");

            var productButton = document.createElement("button");
            $(productButton).attr("class", "add-to-cart-button");
            $(productButton).html("Add to Cart");


            var productInfo = document.createElement("span");
            $(productInfo).attr("class", "info_button");
            $(productInfo).html("Info  ");

            var productSizes = document.createElement("span");
            $(productSizes).attr("class", "size_button");
            $(productSizes).html("Sizes  ");

            var productButton1 = document.createElement("span");
            $(productButton1).attr("class", "desc_button");
            $(productButton1).html("Desc");

            var detailInfoT = document.createElement("div");
            $(detailInfoT).attr("class", "detail_Size");
            //  $(detailInfoT).attr("type", "hidden");
            $(detailInfoT).addClass('hide');


            var desc1 = "";
            for (var j = 0; j < product.size.length - 1; j++) {
                desc1 = desc1 + product.size[j] + "<br>";
            }
            desc1 = desc1 + product.size[product.size.length - 1];

            var productDesc1 = document.createElement("p");
            $(productDesc1).html(desc1);

            var desc2 = document.createElement("p");
            $(desc2).attr("class", "desc2");
            $(desc2).html(product.description);

            var detailInfoD = document.createElement("div");
            $(detailInfoD).attr("class", "detail_desc");
            $(detailInfoD).addClass('hide');

            $(detailInfo).append(itemName);
            $(detailInfo).append(productBr);
            $(detailInfo).append(productDesc);
            $(detailInfo).append(productBr3);
            $(detailInfo).append(productButton);

            $(detailInfoT).append(desc1);
            $(detailInfoD).append(desc2);


            $(detailView).append(closeX);
            $(detailView).append(productImage);
            $(detailView).append(productInfo);
            $(detailView).append(productSizes);
            $(detailView).append(productButton1);
            $(detailView).append(detailInfo);
            $(detailView).append(detailInfoT);
            $(detailView).append(detailInfoD);

            //adding products to list
            if ((selected_category_brojac % 4) == 0) {
                $(productList).append(productItem);
                $(productList).append(detailView);
            }
            else {
                $(productItem).insertAfter(prevProductItem);
                $(detailView).insertAfter(prevDetailView);
            }
            prevProductItem = $(productItem);
            prevDetailView = $(detailView);
            selected_category_brojac++;
        }
    }

    jQuery("#wrapul").append(productList);


    $("#wrap li").click(function (e) {
        OnProductClick($(this), e);
        $('#wrap .detail-view .detail_Size').addClass('hide');
        $('#wrap .detail-view .detail_desc').addClass('hide');
    });

    $(".add-to-cart-button").click(function () {
        OnAddToCart($(this));
    });
    $(".info_button").click(function (target) {

        $('#wrap .detail-view .detail_info').slideDown();
        $('#wrap .detail-view .detail_desc').slideUp();
        //$('#wrap .detail-view .detail_Size').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideUp();

    });
    $(".size_button").click(function () {
        $('#wrap .detail-view .detail_info').slideUp();
        $('#wrap .detail-view .detail_desc').slideUp();
        $('#wrap .detail-view .detail_Size').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideDown();


    });

    $(".desc_button").click(function (target) {

        $('#wrap .detail-view .detail_info').slideUp();
        $('#wrap .detail-view .detail_desc').slideDown();
        $('#wrap .detail-view .detail_desc').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideUp();

    });

    $(".close a").click(function () {

        $('#wrap .detail-view').slideUp('slow');

    });
}

//funkcija koja se poziva za slucaj uspeha poziva metoda GetProductList WCF servisa
function GetProductListSucceeded(result) {

    //alert(result);

    var productList = document.createElement("ul");
    var prevProductItem;
    var prevDetailView;
    var new_brojac = 0;

    
   
    for (var i = result.length - 1; i >= result.length - 12; i--) {

        try {
            var product = eval('(' + result[i] + ')');
        }
        catch (exception) {
            return;
        }

        var productItem = document.createElement("li");

        $(productItem).attr("id", i + 1);
        //$(productItem).html(product.name);
        $(productItem).attr("id1", product._id);

        var productImage = document.createElement("img");
        $(productImage).attr("src", "Images/" + product.imageURL);
        $(productImage).attr("class", "items");
        $(productImage).attr("height", 100);
        $(productImage).attr("alt", "");

        if (i == result.length - 1) {
            NoviId = product._id + 1;
        }

        var productBr = document.createElement("br");
        $(productBr).attr("clear", "all");

        var productDiv = document.createElement("div");
        $(productDiv).html(product.name);
        $(productDiv).attr("id", product._id);

        $(productItem).append(productImage);
        $(productItem).append(productBr);
        $(productItem).append(productDiv);

        var detailView = document.createElement("div");
        $(detailView).attr("class", "detail-view");
        $(detailView).attr("id", "detail-" + (i + 1).toString());


        var closeX = document.createElement("div");
        $(closeX).attr("class", "close");
        $(closeX).attr("align", "right");

        var closeXA = document.createElement("a");
        $(closeXA).html("x");
        $(closeXA).attr("href", "javascript:void(0)");
        $(closeX).append(closeXA);

        var productImage = document.createElement("img");
        $(productImage).attr("src", "Images/" + product.imageURL);
        $(productImage).attr("class", "detail_images");
        $(productImage).attr("width", 300);
        $(productImage).attr("height", 300);
        $(productImage).attr("alt", "");

        var detailInfo = document.createElement("div");
        $(detailInfo).attr("class", "detail_info");

        var itemName = document.createElement("label");
        $(itemName).attr("class", "item_name");
        $(itemName).html(product.name);

        var productBr = document.createElement("br");
        $(productBr).attr("clear", "all");

        var desc = "";
        for (var j = 0; j < product.tags.length - 1; j++) {
            desc = desc + product.tags[j] + ", ";
        }
        desc = desc + product.tags[product.tags.length - 1];


        var productDesc = document.createElement("p");
        $(productDesc).html(desc);

        var productBr1 = document.createElement("br");
        $(productBr1).attr("clear", "all");

        var productBr2 = document.createElement("br");
        $(productBr2).attr("clear", "all");

        var productSpan = document.createElement("span");
        $(productSpan).attr("class", "price");
        $(productSpan).html(product.price);

        var currSpan = document.createElement("span");
        $(currSpan).attr("class", "currency");
        $(currSpan).html(product.currency);

        $(productDesc).append(productBr1);
        $(productDesc).append(productBr2);
        $(productDesc).append(productSpan);
        $(productDesc).append(currSpan);

        var productBr3 = document.createElement("br");
        $(productBr3).attr("clear", "all");

        var productButton = document.createElement("button");
        $(productButton).attr("class", "add-to-cart-button");
        $(productButton).html("Add to Cart");


        var productInfo = document.createElement("span");
        $(productInfo).attr("class", "info_button");
        $(productInfo).html("Info  ");

       var productSizes = document.createElement("span");
        $(productSizes).attr("class", "size_button");
        $(productSizes).html("Sizes  ");

        var productButton1 = document.createElement("span");
        $(productButton1).attr("class", "desc_button");
        $(productButton1).html("Desc");

        var detailInfoT = document.createElement("div");
        $(detailInfoT).attr("class", "detail_Size");
        //  $(detailInfoT).attr("type", "hidden");
        $(detailInfoT).addClass('hide');
       

        var desc1 = "";
        for (var j = 0; j < product.size.length - 1; j++) {
            desc1 = desc1 + product.size[j] + "<br>";
        }
        desc1 = desc1 + product.size[product.size.length - 1];

        var productDesc1 = document.createElement("p");
        $(productDesc1).html(desc1);

        var desc2 = document.createElement("p");
        $(desc2).attr("class", "desc2");
        $(desc2).html(product.description);

        var detailInfoD = document.createElement("div");
        $(detailInfoD).attr("class", "detail_desc");
        $(detailInfoD).addClass('hide');

        $(detailInfo).append(itemName);
        $(detailInfo).append(productBr);
        $(detailInfo).append(productDesc);
        $(detailInfo).append(productBr3);
        $(detailInfo).append(productButton);

        $(detailInfoT).append(desc1);
        $(detailInfoD).append(desc2);
       

        $(detailView).append(closeX);
        $(detailView).append(productImage);
        $(detailView).append(productInfo);
        $(detailView).append(productSizes);
        $(detailView).append(productButton1);
        $(detailView).append(detailInfo);
        $(detailView).append(detailInfoT);
        $(detailView).append(detailInfoD);

        //adding products to list
        if ((new_brojac % 4) == 0) {
            $(productList).append(productItem);
            $(productList).append(detailView);
        }
        else {
            $(productItem).insertAfter(prevProductItem);
            $(detailView).insertAfter(prevDetailView);
        }
        prevProductItem = $(productItem);
        prevDetailView = $(detailView);
        new_brojac++;
    }

    jQuery("#wrapul").append(productList);


    $("#wrap li").click(function (e) {
        OnProductClick($(this), e);
        $('#wrap .detail-view .detail_Size').addClass('hide');
        $('#wrap .detail-view .detail_desc').addClass('hide');
    });

    $(".add-to-cart-button").click(function () {
        OnAddToCart($(this));
    });
    $(".info_button").click(function (target) {

        $('#wrap .detail-view .detail_info').slideDown();
        $('#wrap .detail-view .detail_desc').slideUp();
        //$('#wrap .detail-view .detail_Size').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideUp();

    });
    $(".size_button").click(function () {
        $('#wrap .detail-view .detail_info').slideUp();
        $('#wrap .detail-view .detail_desc').slideUp();
        $('#wrap .detail-view .detail_Size').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideDown();
       

    });

    $(".desc_button").click(function (target) {

        $('#wrap .detail-view .detail_info').slideUp();
        $('#wrap .detail-view .detail_desc').slideDown();
        $('#wrap .detail-view .detail_desc').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideUp();

    });

    $(".close a").click(function () {

        $('#wrap .detail-view').slideUp('slow');

    });

}

$(document).ready(function () {

    // this is for 2nd row's li offset from top. It means how much offset you want to give them with animation
    single_li_offset = 1000; //200;
    current_opened_box = -1;
    Arrays = new Array();

    GetProductList(0);

    $('.remove').livequery('click', function () {

        var deduct = $(this).parent().children(".shopp-price").find('em').html();
        var prev_charges = $('.cart-total span').html();

        var thisID = $(this).parent().attr('id').replace('each-', '');

        var pos = getpos(Arrays, thisID);
        Arrays.splice(pos, 1, "0")

        prev_charges = parseInt(prev_charges) - parseInt(deduct);
        $('.cart-total span').html(prev_charges);
        $('#total-hidden-charges').val(prev_charges);
        $(this).parent().remove();

    });

    $('#Submit').livequery('click', function () {

        var totalCharge = $('#total-hidden-charges').val();

        $('#cart_wrapper').html('Total Charges: €' + totalCharge);

        return false;

    });

    $('.closeCart').click(function () {

        $('#cart_wrapper').slideUp();

    });

    $('#show_cart').click(function () {

        $('#cart_wrapper').slideToggle('slow');

    });

});

//Pomocna funkcija koja proverava da li se element nalazi u nizu
function include(arr, obj) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == obj) return true;
    }
}

//Pomocna funkcija koja pronalazi poziciju elementa
function getpos(arr, obj) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == obj) return i;
    }
}

var category = document.getElementById("category");

var tShirt = document.getElementById("t_shirt");
var tracksuit = document.getElementById("tracksuit");
var swimwear = document.getElementById("swimwear");
var sweatpants = document.getElementById("sweatpants");
var hoodies = document.getElementById("hoodies");
var footwear = document.getElementById("footwear");
var other = document.getElementById("other");
var allProducts = document.getElementById("all");

function changeCatTShirt() {
    category.innerHTML = "T-Shirt";
    GetProductListOnCategory(0, "t_shirt");
    GetProductListOnSearch(0, "t_shirt");
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

function changeCatTracksuit() {
    category.innerHTML = "Tracksuit";
    GetProductListOnCategory(0, "tracksuit");
    GetProductListOnSearch(0, "tracksuit");
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

function changeCatSwimwear() {
    category.innerHTML = "Swimwear";
    GetProductListOnCategory(0, "swimwear");
    GetProductListOnSearch(0, "swimwear");
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

function changeCatSweatpants() {
    category.innerHTML = "Sweatpants";
    GetProductListOnCategory(0, "sweatpants");
    GetProductListOnSearch(0, "sweatpants");
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

function changeCatHoodies() {
    category.innerHTML = "Hoodies";
    GetProductListOnCategory(0, "hoodies");
    GetProductListOnSearch(0, "hoodies");
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

function changeCatFootwear() {
    category.innerHTML = "Footwear";
    GetProductListOnCategory(0, "footwear");
    GetProductListOnSearch(0, "footwear");
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

function changeCatOther() {
    category.innerHTML = "Other";
    GetProductListOnCategory(0, "other");
    GetProductListOnSearch(0, "other");
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

function changeCatAll() {
    category.innerHTML = "All products";
    GetProductListOnCategory(0, "all");
    GetProductList(0);
    $("html, body").animate({ scrollTop: 400 }, "slow");
}

if (tShirt != null)
    tShirt.addEventListener("click", changeCatTShirt);
if (tracksuit != null)
    tracksuit.addEventListener("click", changeCatTracksuit);
if (swimwear != null)
    swimwear.addEventListener("click", changeCatSwimwear);
if (sweatpants != null)
    sweatpants.addEventListener("click", changeCatSweatpants);
if (hoodies != null)
    hoodies.addEventListener("click", changeCatHoodies);
if (footwear != null)
    footwear.addEventListener("click", changeCatFootwear);
if (other != null)
    other.addEventListener("click", changeCatOther);
if (allProducts != null)
    allProducts.addEventListener("click", changeCatAll);

var brojac = 0;
var prev_cat = null;
$('#category_nav a').click(function () {
    brojac++;
    if (brojac > 1) {
        prev_cat.css('color', 'gray');
        prev_cat.hover(function () { $(this).css('color', 'white'); }, function () { $(this).css('color', 'gray'); });
    }
    prev_cat = $(this);
    $(this).css('color', '#ca1818');
    $(this).hover(function () { $(this).css('color', 'white'); }, function () { $(this).css('color', '#ca1818') });
});

$("#btn_s").keyup(function (event) {
    if (event.keyCode == 13) {
        category.innerHTML = "Search results";
        if (prev_cat != null) {
            prev_cat.css('color', 'gray');
            prev_cat.hover(function () { $(this).css('color', 'white'); }, function () { $(this).css('color', 'gray'); });
        }
        var txt = $('#btn_s').val().toLowerCase();
        GetProductListOnSearch(0, txt);
        $("html, body").animate({ scrollTop: 400 }, "slow");
    }
});

function klik(event) {
    if (event.keyCode == 13) {
        var txt = $('#btn_s2').val().toLowerCase();
        GetProductListOnSearch(0, txt);
    }
}


function GetProductListOnSearch(page, search) {
    $.ajax(
                {
                    type: "GET", //GET or POST or PUT or DELETE verb
                    url: "Service.svc/GetProductList", // Location of the service
                    data: '{"page":' + page + '}', //Data sent to server
                    contentType: "application/json; charset=utf-8", // content type sent to server
                    dataType: "json", //Expected data format from server
                    processdata: true, //True or False
                    success: function (msg) //On Successfull service call
                    {
                        GetProductListOnSearchSucceeded(msg, search)
                    },
                    error: function (result) // When Service call fails
                    {
                        alert("Error loading products " + result.status + " " + result.statusText);
                    }
                }
            );
}

function GetProductListOnSearchSucceeded(result, text_search) {
    var productList = document.createElement("ul");
    var prevProductItem;
    var prevDetailView;

    $("#wrapul").empty();

    var prev_product = -1;

    var text_search_brojac = 0;

    var txts = text_search.split(" ");

    for (var i = 0; i < result.length; i++) {
        try {
            var product = eval('(' + result[i] + ')');
        }
        catch (exception) {
            return;
        }

        for (var k = 0; k < txts.length; k++) {
            if ((product._id != prev_product) && (product.tags.indexOf(txts[k]) > -1)) {
                prev_product = product._id;
                var productItem = document.createElement("li");

                $(productItem).attr("id", i + 1);
                //$(productItem).html(product.name);
                $(productItem).attr("id1", product._id);

                var productImage = document.createElement("img");
                $(productImage).attr("src", "Images/" + product.imageURL);
                $(productImage).attr("class", "items");
                $(productImage).attr("height", 100);
                $(productImage).attr("alt", "");


                var productBr = document.createElement("br");
                $(productBr).attr("clear", "all");

                var productDiv = document.createElement("div");
                $(productDiv).html(product.name);
                $(productDiv).attr("id", product._id);

                $(productItem).append(productImage);
                $(productItem).append(productBr);
                $(productItem).append(productDiv);

                var detailView = document.createElement("div");
                $(detailView).attr("class", "detail-view");
                $(detailView).attr("id", "detail-" + (i + 1).toString());


                var closeX = document.createElement("div");
                $(closeX).attr("class", "close");
                $(closeX).attr("align", "right");

                var closeXA = document.createElement("a");
                $(closeXA).html("x");
                $(closeXA).attr("href", "javascript:void(0)");
                $(closeX).append(closeXA);

                var productImage = document.createElement("img");
                $(productImage).attr("src", "Images/" + product.imageURL);
                $(productImage).attr("class", "detail_images");
                $(productImage).attr("width", 300);
                $(productImage).attr("height", 300);
                $(productImage).attr("alt", "");

                var detailInfo = document.createElement("div");
                $(detailInfo).attr("class", "detail_info");

                var itemName = document.createElement("label");
                $(itemName).attr("class", "item_name");
                $(itemName).html(product.name);

                var productBr = document.createElement("br");
                $(productBr).attr("clear", "all");

                var desc = "";
                for (var j = 0; j < product.tags.length - 1; j++) {
                    desc = desc + product.tags[j] + ", ";
                }
                desc = desc + product.tags[product.tags.length - 1];


                var productDesc = document.createElement("p");
                $(productDesc).html(desc);

                var productBr1 = document.createElement("br");
                $(productBr1).attr("clear", "all");

                var productBr2 = document.createElement("br");
                $(productBr2).attr("clear", "all");

                var productSpan = document.createElement("span");
                $(productSpan).attr("class", "price");
                $(productSpan).html(product.price);

                var currSpan = document.createElement("span");
                $(currSpan).attr("class", "currency");
                $(currSpan).html(product.currency);

                $(productDesc).append(productBr1);
                $(productDesc).append(productBr2);
                $(productDesc).append(productSpan);
                $(productDesc).append(currSpan);

                var productBr3 = document.createElement("br");
                $(productBr3).attr("clear", "all");

                var productButton = document.createElement("button");
                $(productButton).attr("class", "add-to-cart-button");
                $(productButton).html("Add to Cart");


                var productInfo = document.createElement("span");
                $(productInfo).attr("class", "info_button");
                $(productInfo).html("Info  ");

                var productSizes = document.createElement("span");
                $(productSizes).attr("class", "size_button");
                $(productSizes).html("Sizes  ");

                var productButton1 = document.createElement("span");
                $(productButton1).attr("class", "desc_button");
                $(productButton1).html("Desc");

                var detailInfoT = document.createElement("div");
                $(detailInfoT).attr("class", "detail_Size");
                //  $(detailInfoT).attr("type", "hidden");
                $(detailInfoT).addClass('hide');


                var desc1 = "";
                for (var j = 0; j < product.size.length - 1; j++) {
                    desc1 = desc1 + product.size[j] + "<br>";
                }
                desc1 = desc1 + product.size[product.size.length - 1];

                var productDesc1 = document.createElement("p");
                $(productDesc1).html(desc1);

                var desc2 = document.createElement("p");
                $(desc2).attr("class", "desc2");
                $(desc2).html(product.description);

                var detailInfoD = document.createElement("div");
                $(detailInfoD).attr("class", "detail_desc");
                $(detailInfoD).addClass('hide');

                $(detailInfo).append(itemName);
                $(detailInfo).append(productBr);
                $(detailInfo).append(productDesc);
                $(detailInfo).append(productBr3);
                $(detailInfo).append(productButton);

                $(detailInfoT).append(desc1);
                $(detailInfoD).append(desc2);


                $(detailView).append(closeX);
                $(detailView).append(productImage);
                $(detailView).append(productInfo);
                $(detailView).append(productSizes);
                $(detailView).append(productButton1);
                $(detailView).append(detailInfo);
                $(detailView).append(detailInfoT);
                $(detailView).append(detailInfoD);

                //adding products to list
                if ((text_search_brojac % 4) == 0) {
                    $(productList).append(productItem);
                    $(productList).append(detailView);
                }
                else {
                    $(productItem).insertAfter(prevProductItem);
                    $(detailView).insertAfter(prevDetailView);
                }
                prevProductItem = $(productItem);
                prevDetailView = $(detailView);
                text_search_brojac++;
            }
        }
    }

    jQuery("#wrapul").append(productList);


    $("#wrap li").click(function (e) {
        OnProductClick($(this), e);
        $('#wrap .detail-view .detail_Size').addClass('hide');
        $('#wrap .detail-view .detail_desc').addClass('hide');
    });

    $(".add-to-cart-button").click(function () {
        OnAddToCart($(this));
    });
    $(".info_button").click(function (target) {

        $('#wrap .detail-view .detail_info').slideDown();
        $('#wrap .detail-view .detail_desc').slideUp();
        //$('#wrap .detail-view .detail_Size').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideUp();

    });
    $(".size_button").click(function () {
        $('#wrap .detail-view .detail_info').slideUp();
        $('#wrap .detail-view .detail_desc').slideUp();
        $('#wrap .detail-view .detail_Size').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideDown();


    });

    $(".desc_button").click(function (target) {

        $('#wrap .detail-view .detail_info').slideUp();
        $('#wrap .detail-view .detail_desc').slideDown();
        $('#wrap .detail-view .detail_desc').removeClass('hide');
        $('#wrap .detail-view .detail_Size').slideUp();

    });

    $(".close a").click(function () {

        $('#wrap .detail-view').slideUp('slow');

    });
}

function OnDelete(id) {
    $.ajax(
               {
                   type: "GET",
                   url: "Service.svc/DeleteItem" + id,
                   data: id,
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   processdata: true,
                   success: function (msg) {
                       alert("Product is succesfully deleted!");
                   },
                   error: function (result) {
                       alert("Error deleting product " + result.status + " " + result.statusText);
                   }
               }
           );
}

function AddNewItemToDB() {
    var ime = document.getElementById("name").value;
    var slika = document.getElementById("image").value;
    var kategorija = document.getElementById("cat").value;
    var opis = document.getElementById("desc").value;
    var velicina = document.getElementById("sizes").value;
    var cena = document.getElementById("price").value;
    var curr = document.getElementById("currency").value;

    if (ime != "" && slika != "" && kategorija != "" && opis != "" && velicina != "" && cena != "" && curr != "")
    {
        var name = "" + ime;
        var txts = ime;
        txts = txts.toLowerCase();
        txts = txts.split(" ");
        var cat = "" + kategorija;
        cat = cat.toLowerCase();
        for (i = 0; i < txts.length-1; i++) {
            cat += "," + txts[i];
        }
        var description = "" + opis;
        var s = velicina.toUpperCase();
        var sizes = "" + s;
        s = sizes.split(",");

        var price = "" + cena;
        var curr = "" + curr;

        var data = name + '|' + cat + '|' + description + '|' + s + '|' + price + '|' + curr + '|';

        var url = "" + slika;
        // uploadPicture(url);

        data = data + url + '|';
        data = data + NoviId + '|';

        uploadProduct(data);
        NoviId = NoviId + 1;
        alert("Item is succesfully added!");
      //  window.location = "HtmlPage.html";
    }
    else
        alert("All fields must be filled!");
}

function uploadPicture(url) {
    $.ajax({
        async: false,
        type: "POST",
        url: "Service.svc/SavePicture",
        data: JSON.stringify(url),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
    });
}

function uploadProduct(data) {
    $.ajax({
        async: false,
        type: "POST",
        url: "Service.svc/AddProduct",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        processData: true,
        success: function (msg) 
         {
            alert("Product is succesfully added!");
         }
    });
}

function Save() {
    AddNewItemToDB();
    location.reload();
}

function F1() {
    if (Da == null)
    {
        alert("Select an item to delete!");
        return;
    }
    OnDelete("?id= " + Da);
    location.reload();
}